import unittest

class ListTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.fruits = ["Orange", "Apple", "Banana", "Raspberry"]

    def testAccessByIndex(self):
        assert "Orange" == self.fruits[0]
        assert "Apple" == self.fruits[1]
    
    def testAccessByNegativeIndex(self):
        assert "Banana" == self.fruits[-2]
        
    def testAccessByIndexRange(self):
        assert ["Apple", "Banana"] == self.fruits[1:3]
        
    def testListLength(self):
        self.assertListLengthIs(4)
    
    def testAppendToList(self):
        self.fruits.append("Peach")
        self.assertListLengthIs(5)
        assert "Peach" == self.fruits[4]
        
    def testExtendList(self):
        extension = ["Peach", "Strawberry"]
        self.fruits.extend(extension)
        self.assertListLengthIs(6)
        assert "Peach" == self.fruits[4]
        assert "Strawberry" == self.fruits[5]
    
    def testPop(self):
        popped = self.fruits.pop()
        assert "Raspberry" == popped
        self.assertListLengthIs(3)
        
    def testRemoveFromList(self):
        self.fruits.remove("Apple")
        self.assertListLengthIs(3)
        count = self.fruits.count("Apple")
        assert 0 == count
        
    def testInsertWithIndex(self):
        self.fruits.insert(1, "Blackberry")
        self.assertListLengthIs(5)
        assert "Blackberry" == self.fruits[1]
        assert "Apple" == self.fruits[2]
        
    def testListIteration(self):
        buffer = ""
        for fruit in self.fruits:
            buffer += " " + fruit
            
        assert " Orange Apple Banana Raspberry" == buffer
        
    def assertListLengthIs(self, expectedLength):
        assert expectedLength == len(self.fruits)

if __name__ == '__main__':
    unittest.main()
